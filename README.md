Tau
===
Tau is a program that generates HTML from Markdown, essentially. It's
supposed to be used to generate a static site (most likely a blog)
from Markdown plus some other data (post dates, comments etc). See the
man page for information on how to use it.

Installation
------------
Chances are you already have Perl installed. All you need are some
modules from CPAN:

    $ sudo cpan Config::IniFiles File::Copy::Recursive \
        Text::Markdown Text::Template Text::Trim File::Slurp \
        File::Spec File::Path File::Find

Now you can `sudo make install`, which will install the man page and
tau script. Alternatively, copy the script and man page somewhere you
find convenient, but make sure everything in the `data/` directory is
copied to `/usr/local/share/tau`, or `tau init` won't work. This is
done automatically by the Makefile, so please use that.

Copyright
---------
Copyright (C) 2014-2018 Kaashif Hymabaccus

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see  <http://www.gnu.org/licenses/>.

