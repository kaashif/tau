.PHONY: install uninstall

PREFIX=/usr/local
BINDIR=${PREFIX}/bin
DATADIR=${PREFIX}/share
MANDIR=${PREFIX}/man/man

all:

install: uninstall
	install -m 0555 -o root -g bin tau ${BINDIR}
	install -m 0555 -o root -g bin tau.1 ${MANDIR}1
	mkdir -m 0555 ${DATADIR}/tau
	cp -R data/* ${DATADIR}/tau/

uninstall:
	rm -f ${BINDIR}/tau
	rm -f ${MANDIR}1/tau.1
	rm -rf ${DATADIR}/tau

